---

## Proyecto Omar Cardona - CallCenter para AlMundo.com

Este proyecto es un CallCenter desarrollado para la prueba de almundo.com. Tiene como objetivo poder visualizar la interacción de llamadas con diferentes tipos de usuarios.
En su interior está la documentación (JavaDoc) y el esquema UML.
Está desarrollado con Maven 3.5.2  y Java 1.8.

## Puesta en marcha

Para dar inicio se debe:

1. Descargar el repositorio: git clone https://oacardona@bitbucket.org/publico_dev/ocardonacallcenter.git
2. Ingresar al directorio del proyecto descargado
3. Para generar la documentación ejecutamos: mvn javadoc:javadoc
4. Para correr el proyecto ejecutamos: mvn exec:java -Dexec.mainClass="com.callcenter.App

Si todo salió bien, ya deberia estar en consola el menú de los test unitarios.

