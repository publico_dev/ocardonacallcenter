package com.callcenter;

/**
 * Clase ENUM que sirve para limitar y definir el perfil (tipo) de usuario
 * @author oacardona
 * @version 2017.01.26
 */
public enum TypeUser {
    /**
     * Usuario de tipo operador
     */
    OPERADOR("OPERADOR", 1),
    /**
     * Usuario de tipo supervisor
     */
    SUPERVISOR("SUPERVISOR", 2),
    /**
     * Usuario de tipo director
     */
    DIRECTOR("DIRECTOR", 3);

    private String typeUser;
    private int typeUserLevel;

    /**
     * Contructor privado
     * @param typeUser Nombre del tipo de usuario
     * @param typeUserLevel Nivel jerarquico del tipo de usuario
     */
    private TypeUser(String typeUser, int typeUserLevel) {
        this.typeUser = typeUser;
        this.typeUserLevel = typeUserLevel;
    }
    
    // START GETTERS AND SETTERS
    
    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }
    
    public int getTypeUserLevel() {
        return typeUserLevel;
    }
    
    public void setTypeUserLevel(int typeUserLevel) {
        this.typeUserLevel = typeUserLevel;
    }
    
    // END GETTERS AND SETTERS
}
