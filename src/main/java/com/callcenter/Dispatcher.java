package com.callcenter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que contiene la lógica del negocio (CallCenter). 
 * Permite la getión de usuarios y llamadas. Clase de tipo SINGLENTON
 *
 * @author Omar Augusto Cardona Suarez
 * @version 2017.01.26
 */
public class Dispatcher {

    private static Dispatcher uniqueDispatcher;
    private static final int CALL_NUM_LIMIT = 10;
    private static int callNumOnLine = 0;
    private ArrayList<User> users;

    /**
     * Constructor privado
     * @param callNumOnLine El numero de llamadas que están en línea
     */
    private Dispatcher(int callNumOnLine) {
        Dispatcher.callNumOnLine = callNumOnLine;
    }
    
    // START GETTERS AND SETTERS
    
    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
    
    // END GETTERS AND SETTERS

    /**
     * Método que permite consultar el objeto de tipo Dispatcher
     * @return Dispatcher - La instancia del objeto
     */
    public static Dispatcher getDispatcher() {
        if (uniqueDispatcher == null) {
            uniqueDispatcher = new Dispatcher(callNumOnLine);
        }
        return uniqueDispatcher;
    }

    /**
     * Método que gestiona las llamadas con los usuarios disponibles
     * @param call La llamada entrante
     * @throws java.lang.Exception Error genérico
     */
    public void dispatchCall(Call call) throws Exception {
        
        User user = getUserFree();
        if (user != null) {
            if( callNumOnLine <= CALL_NUM_LIMIT ){
                user.setInCall(true);
                call.startCall(user);
                addCallOnLine();
            } else {
                waitCall(call);
            }
        } else {
            waitCall(call);
        }
        
    }
    
    /**
     * Método que permite poner la llamada en espera
     * @param call La llamada entrante
     * @throws java.lang.Exception Error genérico
     */
    public void waitCall(Call call) throws Exception {
        System.out.println(call.getClientNumber() + " - En espera..." );
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {

        }
        dispatchCall(call);
    }
    
    /**
     * Método que me indica cual usuario debe asignarse a la llamada entrante,
     * Dependiendo de su disponibilidad y su perfil de usuario
     * @return User - El usuario que está libre
     * @throws java.lang.Exception Error genérico
     */
    public User getUserFree() throws Exception{
        User userFree = null;
        
        Collections.sort(
                users, (user1, user2) ->
                Boolean.compare(user2.getInCall(), user1.getInCall()));


        Collections.sort(
                users, (user1, user2) -> 
                        user1.getTypeUser().getTypeUserLevel() - user2.getTypeUser().getTypeUserLevel() );
        
        for (User user : users) {
            if (user.getInCall() == false) {
                userFree = user;
                break;
            }
        }
        return userFree;
    }
    
    /**
     * Método que agrega una llamada a la cantidad de llamadas en línea
     */
    public static void addCallOnLine(){
        callNumOnLine++;
    }
    
    /**
     * Método que remueve una llamada a la cantidad de llamadas en línea
     */
    public static void rmCallOnLine(){
        callNumOnLine--;
        if(callNumOnLine<0)
            callNumOnLine = 0;
    }
}
