package com.callcenter;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase principal del proyecto
 *
 * @author Omar Augusto Cardona Suarez
 * @version 2017.01.26
 */
public class App {

    /**
     * Metodo main que contiene las pruebas unitarias
     * @param args Argumentos de main
     */
    public static void main(String[] args) {

        try {
            // Creacion del personal del CallCenter
            ArrayList<User> users = new ArrayList<>();
            Dispatcher dispatcher = Dispatcher.getDispatcher();
            int optionMenu = 100;
            
            Scanner in = new Scanner ( System.in );

            System.out.println ("----------------------..:: INICIO DEL TEST ::..-----------------------");
            System.out.println ("Por favor seleccione la opción del test que desea ejecutar:\n");
            System.out.println ("1. Consta de 3  OPERADORES y 10 llamadas");
            System.out.println ("2. Consta de 3  OPERADORES, 3 SUPERVISORES y 10 llamadas");
            System.out.println ("3. Consta de 3  OPERADORES, 3 SUPERVISORES, 3 DIRECTORES y 10 llamadas");
            System.out.println ("4. Consta de 10 OPERADORES, 3 SUPERVISORES, 3 DIRECTORES y 10 llamadas");
            System.out.println ("5. Consta de 6  OPERADORES, 2 SUPERVISORES, 2 DIRECTORES y 15 llamadas");
            System.out.println ("----------------------------------------------------------------------");
            System.out.println ("Opción:");
            optionMenu = in.nextInt();

            switch ( optionMenu ) {
                case 1:
                    // Inicializar el personal del CallCenter
                    users.add(new User(TypeUser.OPERADOR,   "User1"));
                    users.add(new User(TypeUser.OPERADOR,   "User2"));
                    users.add(new User(TypeUser.OPERADOR,   "User3"));

                    // Inicializar del Dispatcher
                    dispatcher.setUsers(users);

                    // Inicializar las llamadas al CallCenter
                    dispatcher.dispatchCall(new Call("350 000 0001"));
                    dispatcher.dispatchCall(new Call("350 000 0002"));
                    dispatcher.dispatchCall(new Call("350 000 0003"));
                    dispatcher.dispatchCall(new Call("350 000 0004"));
                    dispatcher.dispatchCall(new Call("350 000 0005"));
                    dispatcher.dispatchCall(new Call("350 000 0006"));
                    dispatcher.dispatchCall(new Call("350 000 0007"));
                    dispatcher.dispatchCall(new Call("350 000 0008"));
                    dispatcher.dispatchCall(new Call("350 000 0009"));
                    dispatcher.dispatchCall(new Call("350 000 0010"));
                    break;
                case 2:
                    // Inicializar el personal del CallCenter
                    users.add(new User(TypeUser.SUPERVISOR, "User1"));
                    users.add(new User(TypeUser.SUPERVISOR, "User2"));
                    users.add(new User(TypeUser.SUPERVISOR, "User3"));
                    
                    users.add(new User(TypeUser.OPERADOR,   "User4"));
                    users.add(new User(TypeUser.OPERADOR,   "User5"));
                    users.add(new User(TypeUser.OPERADOR,   "User6"));
                    
                    
                    // Inicializar del Dispatcher
                    dispatcher.setUsers(users);

                    // Inicializar las llamadas al CallCenter
                    dispatcher.dispatchCall(new Call("350 000 0001"));
                    dispatcher.dispatchCall(new Call("350 000 0002"));
                    dispatcher.dispatchCall(new Call("350 000 0003"));
                    dispatcher.dispatchCall(new Call("350 000 0004"));
                    dispatcher.dispatchCall(new Call("350 000 0005"));
                    dispatcher.dispatchCall(new Call("350 000 0006"));
                    dispatcher.dispatchCall(new Call("350 000 0007"));
                    dispatcher.dispatchCall(new Call("350 000 0008"));
                    dispatcher.dispatchCall(new Call("350 000 0009"));
                    dispatcher.dispatchCall(new Call("350 000 0010"));
                    break;
                case 3:
                    // Inicializar el personal del CallCenter
                    users.add(new User(TypeUser.DIRECTOR,   "User1"));
                    users.add(new User(TypeUser.DIRECTOR,   "User2"));
                    users.add(new User(TypeUser.DIRECTOR,   "User3"));
                    
                    users.add(new User(TypeUser.SUPERVISOR, "User4"));
                    users.add(new User(TypeUser.SUPERVISOR, "User5"));
                    users.add(new User(TypeUser.SUPERVISOR, "User6"));
                    
                    users.add(new User(TypeUser.OPERADOR,   "User7"));
                    users.add(new User(TypeUser.OPERADOR,   "User8"));
                    users.add(new User(TypeUser.OPERADOR,   "User9"));
                    
                    
                    // Inicializar del Dispatcher
                    dispatcher.setUsers(users);

                    // Inicializar las llamadas al CallCenter
                    dispatcher.dispatchCall(new Call("350 000 0001"));
                    dispatcher.dispatchCall(new Call("350 000 0002"));
                    dispatcher.dispatchCall(new Call("350 000 0003"));
                    dispatcher.dispatchCall(new Call("350 000 0004"));
                    dispatcher.dispatchCall(new Call("350 000 0005"));
                    dispatcher.dispatchCall(new Call("350 000 0006"));
                    dispatcher.dispatchCall(new Call("350 000 0007"));
                    dispatcher.dispatchCall(new Call("350 000 0008"));
                    dispatcher.dispatchCall(new Call("350 000 0009"));
                    dispatcher.dispatchCall(new Call("350 000 0010"));
                    break;
                case 4:
                    // Inicializar el personal del CallCenter
                    users.add(new User(TypeUser.SUPERVISOR, "User1"));
                    users.add(new User(TypeUser.SUPERVISOR, "User2"));
                    users.add(new User(TypeUser.SUPERVISOR, "User3"));
                    
                    users.add(new User(TypeUser.DIRECTOR,   "User4"));
                    users.add(new User(TypeUser.DIRECTOR,   "User5"));
                    users.add(new User(TypeUser.DIRECTOR,   "User6"));
                    
                    users.add(new User(TypeUser.OPERADOR,   "User7"));
                    users.add(new User(TypeUser.OPERADOR,   "User8"));
                    users.add(new User(TypeUser.OPERADOR,   "User9"));
                    users.add(new User(TypeUser.OPERADOR,   "User10"));
                    users.add(new User(TypeUser.OPERADOR,   "User11"));
                    users.add(new User(TypeUser.OPERADOR,   "User12"));
                    users.add(new User(TypeUser.OPERADOR,   "User13"));
                    users.add(new User(TypeUser.OPERADOR,   "User14"));
                    users.add(new User(TypeUser.OPERADOR,   "User15"));
                    users.add(new User(TypeUser.OPERADOR,   "User16"));
                    
                    
                    // Inicializar del Dispatcher
                    dispatcher.setUsers(users);

                    // Inicializar las llamadas al CallCenter
                    dispatcher.dispatchCall(new Call("350 000 0001"));
                    dispatcher.dispatchCall(new Call("350 000 0002"));
                    dispatcher.dispatchCall(new Call("350 000 0003"));
                    dispatcher.dispatchCall(new Call("350 000 0004"));
                    dispatcher.dispatchCall(new Call("350 000 0005"));
                    dispatcher.dispatchCall(new Call("350 000 0006"));
                    dispatcher.dispatchCall(new Call("350 000 0007"));
                    dispatcher.dispatchCall(new Call("350 000 0008"));
                    dispatcher.dispatchCall(new Call("350 000 0009"));
                    dispatcher.dispatchCall(new Call("350 000 0010"));
                    break;
                case 5:
                    // Inicializar el personal del CallCenter
                    users.add(new User(TypeUser.SUPERVISOR, "User1"));
                    users.add(new User(TypeUser.SUPERVISOR, "User2"));
                    
                    users.add(new User(TypeUser.DIRECTOR,   "User3"));
                    users.add(new User(TypeUser.DIRECTOR,   "User4"));
                    
                    users.add(new User(TypeUser.OPERADOR,   "User5"));
                    users.add(new User(TypeUser.OPERADOR,   "User6"));
                    users.add(new User(TypeUser.OPERADOR,   "User7"));
                    users.add(new User(TypeUser.OPERADOR,   "User8"));
                    users.add(new User(TypeUser.OPERADOR,   "User9"));
                    users.add(new User(TypeUser.OPERADOR,   "User10"));
                    
                    
                    // Inicializar del Dispatcher
                    dispatcher.setUsers(users);

                    // Inicializar las llamadas al CallCenter
                    dispatcher.dispatchCall(new Call("350 000 0001"));
                    dispatcher.dispatchCall(new Call("350 000 0002"));
                    dispatcher.dispatchCall(new Call("350 000 0003"));
                    dispatcher.dispatchCall(new Call("350 000 0004"));
                    dispatcher.dispatchCall(new Call("350 000 0005"));
                    dispatcher.dispatchCall(new Call("350 000 0006"));
                    dispatcher.dispatchCall(new Call("350 000 0007"));
                    dispatcher.dispatchCall(new Call("350 000 0008"));
                    dispatcher.dispatchCall(new Call("350 000 0009"));
                    dispatcher.dispatchCall(new Call("350 000 0010"));
                    dispatcher.dispatchCall(new Call("350 000 0011"));
                    dispatcher.dispatchCall(new Call("350 000 0012"));
                    dispatcher.dispatchCall(new Call("350 000 0013"));
                    dispatcher.dispatchCall(new Call("350 000 0014"));
                    dispatcher.dispatchCall(new Call("350 000 0015"));
                    break;
                default:
                    System.err.println ( "Opción no Válida" );
                    break;
            }
            
            
            
        } catch (Exception ex) {
            System.out.println(
                    "Señor usuario, en este momento estamos presentando inconvenietes, "
                    + "por favor intente comunicarse más tarde. Gracias");
        }
        
    }

}
