package com.callcenter;

/**
 * Clase que permite la getión de objetos de tipo llamada
 *
 * @author Omar Augusto Cardona Suarez
 * @version 2017.01.26
 */
public class Call extends Thread {

    private static final int CALL_TIME_MIN = 5;     // Tiempo minimo de llamada en minutos
    private static final int CALL_TIME_MAX = 10;    // Tiempo maximo de llamada en minutos
    private final int ranNum;
    private String clientNumber;
    private User userCallCenter;

    /**
     * Constructor único de clase Call
     * @param clientNumber El número telefónico del cliente
     */
    public Call(String clientNumber) {
        this.clientNumber = clientNumber;
        ranNum = (int) (Math.random() * ((CALL_TIME_MAX + 1) - CALL_TIME_MIN) + CALL_TIME_MIN);
    }
    

    // START GETTERS AND SETTERS
    
    public String getClientNumber() {
        return clientNumber;
    }
    
    public void setClientNumber(String clientNumber) {    
        this.clientNumber = clientNumber;
    }

    public User getUserCallCenter() {
        return userCallCenter;
    }

    public void setUserCallCenter(User userCallCenter) {
        this.userCallCenter = userCallCenter;
    }
    
    // END GETTERS AND SETTERS

    /**
     * Método que permite asignarle un usuario a una llamada
     * @param userCallCenter Usuario que se asigna a la llamada
     */
    public void startCall(User userCallCenter) {
        this.userCallCenter = userCallCenter;
        this.start();
    }

    /**
     * Método sobre-escrito de la ejecución del hilo
     */
    @Override
    public void run() {
        try {
            
            System.out.println(clientNumber
                    + " - Está siendo atendida por el " + userCallCenter.getTypeUser()
                    + " " + userCallCenter.getName());

            Thread.sleep(ranNum * 1000);
            
            userCallCenter.setInCall(false);
            Dispatcher.rmCallOnLine();
            System.out.println(clientNumber + " - Fin de llamada. Duración de " + ranNum + "segundos");
            
        } catch (InterruptedException e) {
            System.out.println(clientNumber + " - La llamada se desconectó. ");
        }
    }

}
