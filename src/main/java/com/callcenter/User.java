package com.callcenter;

/**
 * Clase que permite la getión de objetos de tipo usuario
 *
 * @author Omar Augusto Cardona Suarez
 * @version 2017.01.26
 */
public class User {

    private TypeUser typeUser;
    private String name;
    private boolean inCall = false; // Indica si el susuario está en llamada

    /**
     * Constructor único de clase User
     * @param typeUser El tipo de usuario
     * @param name El nombre del usuario
     */
    public User(TypeUser typeUser, String name) {
        this.typeUser = typeUser;
        this.name = name;
        //this.inCall = Math.random() < 0.5;
    }

    // START GETTERS AND SETTERS
    
    public TypeUser getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(TypeUser typeUser) {
        this.typeUser = typeUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getInCall() {
        return inCall;
    }

    public void setInCall(boolean inCall) {
        this.inCall = inCall;
    }
    
    // END GETTERS AND SETTERS

}
